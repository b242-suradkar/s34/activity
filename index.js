const express = require("express");
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.get("/", (req, res) => {
	res.send("Hello world!");
})


app.get("/hello", (req, res) => {
	res.send("Hello from the /hello endpoint!");
})

// POST Route
app.post("/hello", (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
})


let users = [];

// POST Request to register a user.
app.post("/signup", (req, res) => {
	console.log(req.body);

	if(req.body.username !== '' && req.body.password !== ''){
		// Stores the user object to the users array created
		users.push(req.body);

		res.send(`User ${req.body.username} successfully registered!`);
	}
	else{
		res.send("Please input BOTH username and password.");
	}

})


app.put("/change-password", (req, res) => {
	
	let message;	
	for(let i = 0; i < users.length; i++){

		if(req.body.username == users[i].username){
			
			users[i].password = req.body.password;
			message = `User ${req.body.username}'s password has been updated`;
			break;
		}
		else{
			message = "User does not exist.";
		}
	}

	res.send(message);
})


app.get("/home", (req, res) => {
    res.send("Welcome to the home page");
})

app.get("/users", (req, res) => {
    res.send(users);
})

app.delete("/delete-user", (req, res) => { 
	let message;

    if (users.length != 0){

        for(let i = 0; i < users.length; i++){
            if (req.body.username == users[i].username) {
                users.splice(users[i], 1);
                 message = `user ${req.body.username} has been deleted.`;

                break;

            } 

        }

    } else {
        message = "User does not exist.";

    }
	 res.send(message);

})



app.listen(port, () => console.log(`Server running at port ${port}`));